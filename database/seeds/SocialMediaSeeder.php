<?php

use Illuminate\Database\Seeder;

class SocialMediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('social_media')->delete();

        App\SocialMedia::create(['name' => 'Facebook', 'alias' => 'FACEBOOK']);
        App\SocialMedia::create(['name' => 'Twitter', 'alias' => 'TWITTER']);
    }
}
