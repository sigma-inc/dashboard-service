<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        App\User::create([
            'name' => 'Gustavo Sales Santos',
            'email' => 'gustavosalessantos@gmail.com',
            'phone' => '5511952978616',
            'token' => base64_encode(sha1(uniqid(rand(), true)) . ':' . uniqid(rand(), true)),
            'secret_token' => sha1(uniqid(rand(), true)),
            'profile_picture' => null,
            'level' => 1,
            'xp' => 0,
            'password' => 'e10adc3949ba59abbe56e057f20f883e',
            'born_date' => new DateTime(),
            'type_id' => 2,
            'gender_id' => 1,
            'status_id' => 1
        ]);
    }
}
