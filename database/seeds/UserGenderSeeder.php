<?php

use Illuminate\Database\Seeder;

class UserGenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_genders')->delete();

        App\UserGender::create(['gender' => 'Masculino', 'alias' => 'MALE']);
        App\UserGender::create(['gender' => 'Feminino', 'alias' => 'FEMALE']);
    }
}
