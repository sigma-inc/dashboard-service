<?php

use Illuminate\Database\Seeder;

class ReportTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('report_types')->delete();

        App\ReportType::create(['name' => 'Dia de Trabalho', 'alias' => 'WORKDAY']);
        App\ReportType::create(['name' => 'Questionário Inicial', 'alias' => 'INITIAL_QUESTIONS']);
        App\ReportType::create(['name' => 'Transações', 'alias' => 'TRANSACTIONS']);
        App\ReportType::create(['name' => 'Compra de Itens de Mercado', 'alias' => 'MARKET_ITEMS']);
    }
}
