<?php
use Illuminate\Database\Seeder;

class CountryStateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brazilId = 30;

        $states = [
            [
                "name"       => "Acre",
                "code"       => "AC",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Alagoas",
                "code"       => "AL",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Amapá",
                "code"       => "AP",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Amazonas",
                "code"       => "AM",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Bahia",
                "code"       => "BA",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Ceará",
                "code"       => "CE",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Distrito Federal",
                "code"       => "DF",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Espírito Santo",
                "code"       => "ES",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Goiás",
                "code"       => "GO",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Maranhão",
                "code"       => "MA",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Mato Grosso",
                "code"       => "MT",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Mato Grosso do Sul",
                "code"       => "MS",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Minas Gerais",
                "code"       => "MG",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Pará",
                "code"       => "PA",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Paraíba",
                "code"       => "PB",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Paraná",
                "code"       => "PR",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Pernambuco",
                "code"       => "PE",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Piauí",
                "code"       => "PI",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Rio de Janeiro",
                "code"       => "RJ",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Rio Grande do Norte",
                "code"       => "RN",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Rio Grande do Sul",
                "code"       => "RS",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Rondônia",
                "code"       => "RO",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Roraima",
                "code"       => "RR",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Santa Catarina",
                "code"       => "SC",
                "country_id" => $brazilId
            ],
            [
                "name"       => "São Paulo",
                "code"       => "SP",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Sergipe",
                "code"       => "SE",
                "country_id" => $brazilId
            ],
            [
                "name"       => "Tocantins",
                "code"       => "TO",
                "country_id" => $brazilId
            ]
        ];

        DB::table("country_states")->insert($states);
    }
}