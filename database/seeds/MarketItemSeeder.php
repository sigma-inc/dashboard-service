<?php

use Illuminate\Database\Seeder;

class MarketItemSeeder extends Seeder
{
    const TYPE_FOOD = 1;
    const TYPE_LOCAL_EXPANSION = 2;
    const TYPE_EMPLOYEE = 3;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('market_items')->delete();

        $items = [
            [
                'name' => 'X-Iniciante',
                'min_price' => 8.00,
                'max_price' => 15.00,
                'description' => 'Esse é para quem não tem muita fome, Um hamburger mais simples, porém, excelente.',
                'image' => '',
                'market_item_type_id' => self::TYPE_FOOD
            ],
            [
                'name' => 'X-Queridão',
                'min_price' => 9.00,
                'max_price' => 17.00,
                'description' => 'É o lanche que a galera mais pede. O que fez seu restaurante se tornar famoso.',
                'image' => '',
                'market_item_type_id' => self::TYPE_FOOD
            ],
            [
                'name' => 'X-UTI',
                'min_price' => 25.00,
                'max_price' => 45.00,
                'description' => 'Lanche matador! Só o mais fortes comem e voltam para contar história. Porém muitos pessoas se arriscam a come-lo.',
                'image' => '',
                'market_item_type_id' => self::TYPE_FOOD
            ],
            [
                'name' => 'Dogão Simplão',
                'min_price' => 15.00,
                'max_price' => 23.00,
                'description' => 'Simples só no nome, ele vai superar suas expectativas.',
                'image' => '',
                'market_item_type_id' => self::TYPE_FOOD
            ],
            [
                'name' => 'Super Dog',
                'min_price' => 19.00,
                'max_price' => 27.00,
                'description' => 'Muito procurado pelos clientes, esse dog faz com que existam várias fotos no instagram.',
                'image' => '',
                'market_item_type_id' => self::TYPE_FOOD
            ],
            [
                'name' => 'Fatia Matadora',
                'min_price' => 7.00,
                'max_price' => 12.00,
                'description' => 'Uma mega fatia de pizza que faz inveja a quem pediu outro lanche.',
                'image' => '',
                'market_item_type_id' => self::TYPE_FOOD
            ],
            [
                'name' => 'Aquela Pizza',
                'min_price' => 25.00,
                'max_price' => 45.00,
                'description' => 'Uma pizza enorme que você não sera capaz de comer sozinho, ou será?',
                'image' => '',
                'market_item_type_id' => self::TYPE_FOOD
            ],
            [
                'name' => 'Torta Doce',
                'min_price' => 8.00,
                'max_price' => 15.00,
                'description' => 'Adoce a vida de seus clientes, essa receita de torta veio direto da França e faz com que muitos fiquem curiosos para conhece-la.',
                'image' => '',
                'market_item_type_id' => self::TYPE_FOOD
            ],
            [
                'name' => 'Donut',
                'min_price' => 6.00,
                'max_price' => 13.00,
                'description' => 'Já comeu a bomba de chocolate? Essa é três vezes melhor! Pode ter certeza que terão clientes procurando por ela.',
                'image' => '',
                'market_item_type_id' => self::TYPE_FOOD
            ],
            [
                'name' => 'Walter',
                'min_price' => 1800.00,
                'max_price' => 2200.00,
                'description' => 'O Walter é cheio da lábia, consegue mais clientes para você, porém ele demora muito para entregar os pedidos de tanto que conversa.',
                'image' => '',
                'market_item_type_id' => self::TYPE_EMPLOYEE
            ],
            [
                'name' => 'Igão',
                'min_price' => 2800.00,
                'max_price' => 4000.00,
                'description' => 'O Igão é mais quieto. As pessoas acham que ele é revoltado com a vida, mas atende muito bem e consegue fazer entregas mais rápidas.',
                'image' => '',
                'market_item_type_id' => self::TYPE_EMPLOYEE
            ],
            [
                'name' => 'Estou começando a crescer!',
                'min_price' => 10000.00,
                'max_price' => 10000.00,
                'description' => 'Aumente seu restaurante para caber duas vezes mais clientes.',
                'image' => '',
                'market_item_type_id' => self::TYPE_LOCAL_EXPANSION
            ],
            [
                'name' => 'Cabe mais gente aqui?',
                'min_price' => 50000.00,
                'max_price' => 50000.00,
                'description' => 'Aumente seu restaurante para caber três vezes mais clientes.',
                'image' => '',
                'market_item_type_id' => self::TYPE_LOCAL_EXPANSION
            ],
            [
                'name' => 'Chama toda a galera!',
                'min_price' => 100000.00,
                'max_price' => 100000.00,
                'description' => 'Aumente seu restaurante para caber quatro vezes mais clientes.',
                'image' => '',
                'market_item_type_id' => self::TYPE_LOCAL_EXPANSION
            ]
        ];

        foreach ($items as $item) {
            App\MarketItem::create($item);
        }
    }
}
