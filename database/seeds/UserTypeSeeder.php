<?php

use Illuminate\Database\Seeder;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_types')->delete();

        App\UserType::create(['type' => 'Administrador', 'alias' => 'ADMINISTRATOR']);
        App\UserType::create(['type' => 'Player', 'alias' => 'PLAYER']);
    }
}
