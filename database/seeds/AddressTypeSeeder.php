<?php

use Illuminate\Database\Seeder;

class AddressTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('address_types')->delete();

        App\AddressType::create(['name' => 'Residencial', 'alias' => 'RESIDENTIAL']);
        App\AddressType::create(['name' => 'Comercial', 'alias' => 'COMMERCIAL']);
    }
}
