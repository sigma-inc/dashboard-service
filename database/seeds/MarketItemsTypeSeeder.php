<?php

use Illuminate\Database\Seeder;

class MarketItemsTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('market_item_types')->delete();

        App\MarketItemType::create(['name' => 'Comida', 'alias' => 'FOOD']);
        App\MarketItemType::create(['name' => 'Expansão de Local', 'alias' => 'LOCAL_EXPANSION']);
        App\MarketItemType::create(['name' => 'Funcionário', 'alias' => 'EMPLOYEE']);
        App\MarketItemType::create(['name' => 'Produto de Limpeza', 'alias' => 'CLEAN_PRODUCT']);
    }
}
