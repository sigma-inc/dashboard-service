<?php

use Illuminate\Database\Seeder;

class UserStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_statuses')->delete();

        App\UserStatus::create(['status' => 'Ativado', 'alias' => 'ON']);
        App\UserStatus::create(['status' => 'Desativado', 'alias' => 'OFF']);
    }
}
