<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MarketItemsTypeSeeder::class);
        $this->call(MarketItemSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(CountryStateSeeder::class);
        $this->call(AddressTypeSeeder::class);
        $this->call(SocialMediaSeeder::class);
        $this->call(ReportTypeSeeder::class);
        $this->call(UserGenderSeeder::class);
        $this->call(UserStatusSeeder::class);
        $this->call(UserTypeSeeder::class);
        $this->call(UserSeeder::class);
    }
}
