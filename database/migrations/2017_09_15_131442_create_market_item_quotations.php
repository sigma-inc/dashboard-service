<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketItemQuotations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('market_item_quotations');

        Schema::create('market_item_quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('price');

            $table->integer('market_item_id')->unsigned();
            $table->foreign('market_item_id')
                ->references('id')
                ->on('market_items');

            $table->integer('workday_id')->unsigned();
            $table->foreign('workday_id')
                ->references('id')
                ->on('workdays');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_item_quotations');
    }
}
