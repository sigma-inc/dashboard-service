<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('user_addresses');

        Schema::create('user_addresses', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->integer('country_id')->unsigned();
            $table->foreign('country_id')
                ->references('id')
                ->on('countries');

            $table->integer('country_state_id')->unsigned();
            $table->foreign('country_state_id')
                ->references('id')
                ->on('country_states');

            $table->integer('address_type_id')->unsigned();
            $table->foreign('address_type_id')
                ->references('id')
                ->on('address_types');

            $table->string('city');
            $table->string('district');
            $table->string('street');
            $table->string('complement')->nullable();
            $table->string('number');
            $table->string('postal_code');
            $table->boolean('is_delivery');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_addresses');
    }
}
