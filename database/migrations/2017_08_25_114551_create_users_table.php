<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('phone');
            $table->dateTime('born_date');
            $table->string('token');
            $table->string('secret_token');
            $table->string('profile_picture')->nullable();
            $table->integer('level');
            $table->float('xp');

            $table->integer('type_id')->unsigned();
            $table->foreign('type_id')
                ->references('id')
                ->on('user_types');

            $table->integer('gender_id')->unsigned();
            $table->foreign('gender_id')
                ->references('id')
                ->on('user_genders');

            $table->integer('status_id')->unsigned();
            $table->foreign('status_id')
                ->references('id')
                ->on('user_statuses');


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
