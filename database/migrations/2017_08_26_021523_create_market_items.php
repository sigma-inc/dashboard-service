<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('market_items');

        Schema::create('market_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->decimal('min_price');
            $table->decimal('max_price');
            $table->string('description')->nullable();
            $table->string('image')->nullable();

            $table->integer('market_item_type_id')->unsigned();
            $table->foreign('market_item_type_id')
                ->references('id')
                ->on('market_item_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('market_items');
    }
}
