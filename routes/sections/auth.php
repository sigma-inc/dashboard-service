<?php

Route::post('auth/login', 'AuthController@login');

Route::get('auth/login/{token}', 'AuthController@loginWithToken');