<?php

Route::post(
    'market-items/buy',
    'MarketItemController@buy'
);

Route::get(
    'market-items/types',
    'MarketItemController@allMarketItemTypes'
);

Route::get(
    'market-items/{workdayNumber}/{marketItemTypeId?}',
    'MarketItemController@allByMarketItemTypeId'
);