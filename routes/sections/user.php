<?php

    Route::get('users/token/{token}', 'UserController@uniqueByToken');

    Route::resource('users', 'UserController');
