<?php

Route::resource(
    'reports',
    'ReportController',
    ['only' => 'store']
);

Route::get(
    'reports/{reportTypeId}/{userId?}',
    'ReportController@all'
);