<?php

Route::post(
    'workdays',
    'WorkdayController@store'
);

Route::get(
    'workdays/last/{userId?}',
    'WorkdayController@lastByUserId'
);

Route::get(
    'workdays/{userId?}',
    'WorkdayController@allByUserId'
);