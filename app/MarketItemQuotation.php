<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketItemQuotation extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'price',
        'workday_id',
        'market_item_id'
    ];

    public function workday()
    {
        return $this->belongsTo('App\Workday');
    }
}
