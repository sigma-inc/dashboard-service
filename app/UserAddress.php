<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $fillable = [
        'country_id',
        'country_state_id',
        'address_type_id',
        'city',
        'district',
        'street',
        'complement',
        'number',
        'postal_code',
        'is_delivery'
    ];

    public function userAddresses()
    {
        return $this->belongsTo('App\User');
    }
}
