<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGender extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'gender',
        'alias'
    ];
}
