<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'code',
        'name'
    ];

    public function countries()
    {
        return $this->hasMany('App\Country');
    }
}
