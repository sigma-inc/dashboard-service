<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryState extends Model
{
    protected $fillable = [
        'code',
        'name'
    ];

    public function countryStates()
    {
        return $this->belongsTo('App\Country');
    }
}
