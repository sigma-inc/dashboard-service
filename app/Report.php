<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
        'data',
        'report_type_id',
        'user_id'
    ];

    public function type()
    {
        return $this->belongsTo('App\ReportType', 'report_type_id');
    }
}
