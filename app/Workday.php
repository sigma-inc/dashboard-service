<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workday extends Model
{
    protected $fillable = [
        'type',
        'number',
        'value',
        'user_id'
    ];

    public function workdays()
    {
        return $this->belongsTo('App\User');
    }
}
