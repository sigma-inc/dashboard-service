<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressType extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'alias'
    ];

    public function addressTypes()
    {
        return $this->hasMany('App\AddressType');
    }
}
