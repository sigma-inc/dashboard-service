<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function socialMedias()
    {
        return $this->hasMany('App\SocialMedia');
    }
}
