<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSocialMedia extends Model
{
    protected $fillable = [
        'username',
        'token',
        'secret_token',
        'social_media_id'
    ];

    public function userSocialMedias()
    {
        return $this->belongsTo('App\User');
    }
}
