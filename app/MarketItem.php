<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketItem extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'min_price',
        'max_price',
        'description',
        'image',
        'market_item_type_id'
    ];

    public function type()
    {
        return $this->belongsTo('App\MarketItemType');
    }

    public function quotations()
    {
        return $this->hasMany('App\MarketItemQuotation');
    }

    public function lastQuotation()
    {
        return $this->hasOne('App\MarketItemQuotation')
            ->orderBy('workday_id', 'desc');
    }
}
