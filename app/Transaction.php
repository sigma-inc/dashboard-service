<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'market_item_id',
        'user_id',
        'value',
        'delivery_time',
        'rate'
    ];

    public function marketItem()
    {
        return $this->belongsTo('App\MarketItem');
    }
}
