<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportType extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'alias'
    ];
}
