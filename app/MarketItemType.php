<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketItemType extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'alias'
    ];

    public function marketItems()
    {
        return $this->hasMany('App\MarketItem');
    }
}
