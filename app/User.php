<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'name',
        'email',
        'phone',
        'token',
        'secret_token',
        'profile_picture',
        'level',
        'xp',
        'type_id',
        'status_id',
        'gender_id',
        'password',
        'born_date',
        'deleted_at'
    ];

    protected $hidden = [
        'password',
        'token',
        'secret_token'
    ];

    protected $dates = [
        'born_date',
        'deleted_at'
    ];

    public function status()
    {
        return $this->belongsTo('App\UserStatus');
    }

    public function gender()
    {
        return $this->belongsTo('App\UserGender');
    }

    public function type()
    {
        return $this->belongsTo('App\UserType');
    }

    public function lastWorkday()
    {
        return $this->hasOne('App\Workday', 'user_id', 'id')
            ->orderBy('id', 'desc');
    }
}
