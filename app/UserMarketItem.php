<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMarketItem extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'market_item_id',
        'quantity'
    ];

    public function userMarketItems()
    {
        return $this->belongsTo('App\User');
    }
}
