<?php

namespace App\Http\Shared;

class ResponseLog
{
    public static $faults = [];
    public static $errors = [];

    public static function clear()
    {
        ResponseLog::$faults = [];
        ResponseLog::$errors = [];
    }

    public static function setFault($fault)
    {
        ResponseLog::$faults[] = $fault;

        return false;
    }

    public static function setError($error)
    {
        ResponseLog::$errors[] = $error;

        return false;
    }

    public static function hasFaults()
    {
        return count(ResponseLog::$faults) > 0;
    }

    public static function hasErrors()
    {
        return count(ResponseLog::$errors) > 0;
    }

    public static function hasFaultsOrErrors()
    {
        return ResponseLog::hasFaults() || ResponseLog::hasErrors();
    }
}