<?php

namespace App\Http\Services;

use App\Http\Middleware\JwtVerification;
use App\MarketItem;
use App\MarketItemQuotation;
use App\MarketItemType;
use App\Http\Shared\ResponseLog;
use App\UserMarketItem;

class MarketItemService
{
    /**
     * @var MarketItem
     */
    private $model;

    /**
     * @var MarketItemType
     */
    private $marketItemTypeModel;

    /**
     * @var UserMarketItem
     */
    private $userMarketItemModel;

    /**
     * @var MarketItemQuotation
     */
    private $marketItemQuotationModel;

    function __construct()
    {
        $this->model = MarketItem::with(['type', 'quotations', 'lastQuotation']);
        $this->marketItemTypeModel = new MarketItemType();
        $this->userMarketItemModel = new UserMarketItem();
        $this->marketItemQuotationModel = new MarketItemQuotation();
    }

    public function allByType($marketTypeItemId)
    {
        $marketItems = [];

        try {
            $hasMarketTypeItem = $this->hasMarketItemTypeById($marketTypeItemId);

            if (!$hasMarketTypeItem) {
                return ResponseLog::setFault('Tipo de item não existente');
            }

            $marketItems = $this->model
                ->where('market_item_type_id', $marketTypeItemId)
                ->get();
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }

        return $marketItems;
    }

    public function allMarketItemTypes()
    {
        $marketItemTypes = [];

        try {
            $marketItemTypes = $this->marketItemTypeModel
                ->get();

            if (!$marketItemTypes) {
                return ResponseLog::setFault('Nenhum tipo de item encontrado');
            }
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }

        return $marketItemTypes;
    }

    public function buyMarketItem($data)
    {
        try {
            $user = JwtVerification::$user;

            //  First data
            $data['user_id'] = $user['id'];

            $user = new UserMarketItem();
            $user->fill($data);
            $user->save();
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }
    }

    public function createQuotation($workdayId)
    {
        try {
            $marketItems = $this->model->select(['id', 'min_price', 'max_price'])
                ->get();

            $quotations = [];

            foreach ($marketItems as $marketItem) {
                $marketItem = $marketItem['attributes'];

                $data = [
                    'market_item_id' => $marketItem['id'],
                    'price' => rand($marketItem['min_price'], $marketItem['max_price']),
                    'workday_id' => $workdayId
                ];

                $quotations[] = $data;
            }

            $marketItemQuotation = new MarketItemQuotation();
            $marketItemQuotation->insert($quotations);
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }
    }

    private function hasMarketItemTypeById($marketItemTypeId)
    {
        return $this->marketItemTypeModel
            ->where('id', $marketItemTypeId)
            ->count('id');
    }
}