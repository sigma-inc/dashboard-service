<?php

namespace App\Http\Services;

use App\Http\Shared\ResponseLog;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha256;


class AuthService
{
    /**
     * @var UserService
     */
    private $user;

    function __construct()
    {
        $this->user = new UserService();
    }

    public function authenticate($email, $password)
    {
        if(is_null($email) || is_null($password)) {
            ResponseLog::setFault('Usuário ou senha inválidos');

            return false;
        }

        $user = $this->user->uniqueByEmailAndPassword($email, $password);

        if(!$user) {
            ResponseLog::clear();
            ResponseLog::setFault('Usuário ou senha inválidos');

            return false;
        }

        session(['user' => $user]);

        $token = $this->getToken($user);

        return $token;
    }

    public function authenticateWithUserToken($userToken)
    {
        if(is_null($userToken)) {
            ResponseLog::setFault('Usuário inválido');

            return false;
        }

        $user = $this->user->uniqueByToken($userToken);

        if(!$user) {
            ResponseLog::clear();
            ResponseLog::setFault('Usuário inválido');

            return false;
        }

        $token = $this->getToken($user);

        return $token;
    }

    private function getToken($user)
    {
        $appSecretKey = config('app.secret_key');
        $appUrl = config('app.url');

        $signer = new Sha256();

        $token = (new Builder())->setIssuer($appUrl)
            ->setId($appSecretKey, true)
            ->setIssuedAt(time())
            ->setNotBefore(time())
            ->setExpiration(time() + 3600)
            ->set('user', [
                'id' => $user['id']
            ])
            ->sign($signer, $user['secret_token'])
            ->getToken();

        return $token;
    }
}