<?php

namespace App\Http\Services;

use App\Http\Middleware\JwtVerification;
use App\Http\Shared\ResponseLog;
use App\Report;
use App\ReportType;

class ReportService
{
    const REPORT_TYPE_WORKDAY = 1;
    const REPORT_TYPE_TRANSACTION = 3;

    private $model;
    private $reportTypeModel;
    private $workdayService;
    private $relations;

    function __construct()
    {
        $this->relations = ['type'];
        $this->model = Report::with($this->relations);
        $this->reportTypeModel = ReportType::with([]);
        $this->workdayService = new WorkdayService();
    }

    public function all($reportTypeId, $userId)
    {
        $data = [];

        try {
            if ($reportTypeId == self::REPORT_TYPE_WORKDAY) {
                $data = $this->getWorkdays($userId);
            }
            elseif ($reportTypeId == self::REPORT_TYPE_TRANSACTION){
                // TODO
            } else {
                $data = $this->getReport($userId, $reportTypeId);
            }

            if (!$data) {
                ResponseLog::setFault('Não existem relatórios cadastrados para esse usuário');
            }
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }

        return $data;
    }

    public function create($data)
    {
        try {
            $reportType = $this->uniqueReportTypeById($data['report_type_id']);

            if (!$reportType) {
                return false;
            }

            if ($reportType['id'] == self::REPORT_TYPE_WORKDAY || $reportType['id'] == self::REPORT_TYPE_TRANSACTION) {
                return ResponseLog::setFault('Tipo de relatório inválido para criação');
            }

            $user = JwtVerification::$user;

            //  First data
            $data['user_id'] = $user['id'];
            $data['data'] = json_encode($data['data']);

            $workday = new Report();
            $workday->fill($data);
            $workday->save();
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }
    }

    private function uniqueReportTypeById(int $reportTypeId)
    {
        $reportType = [];

        try {
            $reportType = $this->reportTypeModel->find($reportTypeId);

            if (!$reportType) {
                ResponseLog::setFault('Tipo de relatório não encontrado');

                return false;
            }
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }

        return $reportType;
    }

    private function getWorkdays($userId)
    {
        return $this->workdayService->allByUserId($userId);
    }

    private function getReport($userId, $reportTypeId)
    {
        return $this->model
            ->where([
                'user_id' => $userId,
                'report_type_id' => $reportTypeId
            ])
            ->get();
    }
}