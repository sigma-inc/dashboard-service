<?php

namespace App\Http\Services;


use App\Http\Shared\ResponseLog;
use App\Workday;

class WorkdayService
{
    const WORKDAY_START = 'START';
    const WORKDAY_END = 'END';

    private $model;

    /**
     * @var MarketItemService
     */
    private $marketItemService;

    function __construct()
    {
        $this->model = Workday::with([]);
        $this->marketItemService = new MarketItemService();
    }

    public function allByUserId($userId)
    {
        $workdays = [];

        try {
            if (!$userId) {
                return ResponseLog::setFault('Usuário não encontrado');
            }

            $workdays = $this->model
                ->where(['user_id' => $userId])
                ->get();

            if (!$workdays) {
                return ResponseLog::setFault('Não existem dias de trabalho cadastrados para esse usuário');
            }
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }

        return $workdays;
    }

    public function lastByUserId($userId)
    {
        $workday = [];

        try {
            if (!$userId) {
                return ResponseLog::setFault('Usuário não encontrado');
            }

            $workday = $this->model
                ->where(['user_id' => $userId])
                ->orderBy('id', 'desc')
                ->first();

            if (!$workday) {
                ResponseLog::setFault('Dia de trabalho por usuário não encontrado');

                return null;
            }

            $workday = $workday['attributes'];
        }
        catch(\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }

        return $workday;
    }

    public function create($data, $user)
    {
        try {
            //  First data
            $data['user_id'] = $user['id'];
            $data['type'] = self::WORKDAY_START;
            $data['number'] = 1;

            $lastWorkday = $this->lastByUserId($user['id']);

            ResponseLog::clear();

            if (!empty($lastWorkday)) {
                $data['number'] = $lastWorkday['number'];

                if ($lastWorkday['type'] == self::WORKDAY_START) {
                    $data['type'] = self::WORKDAY_END;
                }

                if ($lastWorkday['type'] == self::WORKDAY_END) {
                    $data['number'] = $lastWorkday['number'] + 1;
                }
            }

            $workday = new Workday();
            $workday->fill($data);
            $workday->save();

            //  Create market items quotations
            $lastWorkday = $this->lastByUserId($user['id']);

            if ($lastWorkday['type'] == self::WORKDAY_START) {
                $this->marketItemService->createQuotation($lastWorkday['id']);
            }
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }

        return $data;
    }
}