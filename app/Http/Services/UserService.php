<?php

namespace App\Http\Services;

use App\Http\Shared\ResponseLog;
use App\User;

class UserService
{
    const STATUS_OFF = 2;

    private $model;

    function __construct()
    {
        $this->model = User::with(['status', 'gender', 'type', 'lastWorkday']);
    }

    public function all()
    {
        $users = [];

        try {
            $users = $this->model->get();

            if (!$users) {
                ResponseLog::setFault('Não existem usuários cadastrados');
            }
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }

        return $users;
    }

    public function uniqueById($id)
    {
        $user = [];

        try {
            $user = $this->model->find($id);

            if (!$user) {
                ResponseLog::setFault('Usuário não encontrado');
            }
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }

        return $user;
    }

    public function uniqueByEmailAndPassword($email, $password)
    {
        $user = [];

        try {
            $user = User::with(['status', 'gender', 'type'])
                ->where(['email' => $email, 'password' => md5($password)])
                ->first();

            if (!$user) {
                ResponseLog::setFault('Usuário não encontrado');

                return false;
            }

            $user = $user['attributes'];
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }

        return $user;
    }

    public function uniqueByToken($token)
    {
        $user = [];

        try {
            $user = $this->model
                ->where(['token' => $token])
                ->first();

            if (!$user) {
                ResponseLog::setFault('Usuário não encontrado');

                return false;
            }
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }

        return $user;
    }

    public function create($data)
    {
        try {
            if ($data['password'] != $data['password_confirmation']) {
                ResponseLog::setFault('As senhas não são iguais');

                return false;
            }

            $data['token'] = md5(uniqid(rand(), true));
            $data['password'] = md5($data['password']);

            $user = new User();
            $user->fill($data);
            $user->save();
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }
    }

    public function update($data, $id)
    {
        try {
            $notAllowedFields = ['password', 'token', 'email'];
            $dataKeys = array_keys($data);

            if ($this->hasNotAllowedFields($notAllowedFields, $dataKeys)) {
                ResponseLog::setFault('Campos inválidos');

                return false;
            }

            $user = $this->uniqueById($id);

            if(!empty($user)) {
                $user->fill($data);
                $user->save();
            }
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }
    }

    public function desactivate($id)
    {
        try {
            $user = $this->uniqueById($id);

            if(!empty($user)) {
                $data = [
                    'status_id' => self::STATUS_OFF,
                    'deleted_at' => new \DateTime()
                ];
                $user->fill($data);
                $user->save();
            }
        }
        catch (\Exception $e) {
            ResponseLog::setError($e->getMessage());
        }
    }

    private function hasNotAllowedFields($notAllowedFields, $keys)
    {
        foreach ($keys as $key) {
            if (in_array($key, $notAllowedFields)) {
                return true;
            }
        }

        return false;
    }
}