<?php

namespace App\Http\Middleware;

use App\Http\Shared\ResponseLog;
use Closure;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

class JwtVerification
{
    public static $user = null;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $authorization = $request->header('Authorization');

            $token = (new Parser())->parse((string)$authorization);
            $tokenIsValid = $this->validateToken($token);

            if (!$tokenIsValid) {
                throw new \Exception();
            }

            JwtVerification::$user = (array) $token->getClaim('user');
        }
        catch(\Exception $e) {
            ResponseLog::clear();
            ResponseLog::setError('Token is not valid');
        }

        return $next($request);
    }

    private function validateToken($token)
    {
        $appUrl = config('app.url');
        $appSecretKey = config('app.secret_key');

        $data = new ValidationData();

        $data->setIssuer($appUrl);
        $data->setId($appSecretKey);

        return $token->validate($data);
    }
}
