<?php

namespace App\Http\Controllers;

use App\Http\Services\UserService;
use Illuminate\Http\Request;

class UserController extends JsonController
{
    /**
     * @var UserService
     */
    private $user;

    function __construct()
    {
        parent::__construct();

        $this->middleware('jwt.auth');

        $this->user = new UserService();
    }

    public function index()
    {
        $users = $this->user->all();

        return $this->errorsOrData($users);
    }

    public function show($id)
    {
        $user = $this->user->uniqueById($id);

        return $this->errorsOrData($user);
    }

    public function store(Request $request)
    {
        $this->user->create($request->all());

        return $this->errorsOrEmpty();
    }

    public function update(Request $request, $id)
    {
        $this->user->update($request->all(), $id);

        return $this->errorsOrEmpty();
    }

    public function destroy($id)
    {
        $this->user->desactivate($id);

        return $this->errorsOrEmpty();
    }

    public function uniqueByToken($token)
    {
        $user = $this->user->uniqueByToken($token);

        return $this->errorsOrData($user);
    }
}
