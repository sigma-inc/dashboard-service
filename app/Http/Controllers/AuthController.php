<?php

namespace App\Http\Controllers;

use App\Http\Services\AuthService;
use App\Http\Shared\ResponseLog;
use Illuminate\Http\Request;

class AuthController extends JsonController
{
    /**
     * @var AuthService
     */
    private $auth;

    function __construct()
    {
        parent::__construct();

        $this->auth = new AuthService();
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $data = $this->auth->authenticate($credentials['email'], $credentials['password']);

        if (ResponseLog::hasFaultsOrErrors()) {
            return $this->errorsOrEmpty();
        }

        echo $data;
    }

    public function loginWithToken(string $token)
    {
        $data = $this->auth->authenticateWithUserToken($token);

        if (ResponseLog::hasFaultsOrErrors()) {
            return $this->errorsOrEmpty();
        }

        echo $data;
    }
}
