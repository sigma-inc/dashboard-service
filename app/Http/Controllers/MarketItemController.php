<?php

namespace App\Http\Controllers;

use App\Http\Services\MarketItemService;
use Illuminate\Http\Request;

class MarketItemController extends JsonController
{
    /**
     * @var MarketItemService
     */
    private $marketItem;

    function __construct()
    {
        parent::__construct();

        $this->middleware('jwt.auth');

        $this->marketItem = new MarketItemService();
    }

    /**
     * @param int $marketItemTypeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function allByMarketItemTypeId(int $marketItemTypeId)
    {
        $marketItems = $this->marketItem->allByType($marketItemTypeId);

        return $this->errorsOrData($marketItems);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function allMarketItemTypes()
    {
        $marketItemTypes = $this->marketItem->allMarketItemTypes();

        return $this->errorsOrData($marketItemTypes);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function buy(Request $request)
    {
        $this->marketItem->buyMarketItem($request->only(['quantity', 'market_item_id']));

        return $this->errorsOrEmpty();
    }
}
