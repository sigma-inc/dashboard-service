<?php

namespace App\Http\Controllers;

use App\Http\Shared\ResponseLog;

class JsonController extends Controller
{
    const CODE_SUCCESS = 1;
    const CODE_FAULT = 2;
    const CODE_ERROR = 3;

    function __construct()
    {
        ResponseLog::clear();
    }

    protected function errorsOrEmpty()
    {
        $response = $this->validateResponse();

        $code = $response['code'];
        $data = [];

        if (!is_null($response['errors'])) {
            $data = $response['errors'];
        }

        return response()->json($this->base($code, $data), $response['status'], [], JSON_NUMERIC_CHECK);
    }

    protected function errorsOrData($data)
    {
        $response = $this->validateResponse();

        $code = $response['code'];

        if (!is_null($response['errors'])) {
            $data = $response['errors'];
        }

        return response()->json($this->base($code, $data), $response['status'], [], JSON_NUMERIC_CHECK);
    }

    private function validateResponse()
    {
        $status = 200;
        $code = self::CODE_SUCCESS;
        $errors = null;

        if (ResponseLog::hasFaults()) {
            $code = self::CODE_FAULT;
            $errors = ResponseLog::$faults;
        }

        if (ResponseLog::hasErrors()) {
            $status = 500;
            $code = self::CODE_ERROR;
            $errors = ResponseLog::$errors;
        }

        return [
            'code' => $code,
            'errors' => $errors,
            'status' => $status
        ];
    }

    private function base($code, $data)
    {
        return [
            'status' => $code,
            'data' => $data
        ];
    }
}