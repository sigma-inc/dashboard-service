<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtVerification;
use App\Http\Services\WorkdayService;
use Illuminate\Http\Request;

class WorkdayController extends JsonController
{
    /**
     * @var WorkdayService
     */
    private $workday;

    function __construct(Request $request)
    {
        parent::__construct();

        $this->middleware('jwt.auth');

        $this->workday = new WorkdayService();
    }

    /**
     * @param int|null $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function allByUserId(int $userId = null)
    {
        if (is_null($userId)) {
            $userId = JwtVerification::$user['id'];
        }

        $workdays = $this->workday->allByUserId($userId);

        return $this->errorsOrData($workdays);
    }

    /**
     * @param int|null $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function lastByUserId(int $userId = null)
    {
        if (is_null($userId)) {
            $userId = JwtVerification::$user['id'];
        }

        $workday = $this->workday->lastByUserId($userId);

        return $this->errorsOrData($workday);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function store(Request $request)
    {
        $user = JwtVerification::$user;

        $workday = $this->workday->create($request->only('value'), $user);

        return $this->errorsOrData($workday);
    }
}
