<?php

namespace App\Http\Controllers;

use App\Http\Middleware\JwtVerification;
use App\Http\Services\ReportService;
use Illuminate\Http\Request;

class ReportController extends JsonController
{
    /**
     * @var ReportService
     */
    private $report;

    function __construct()
    {
        parent::__construct();

        $this->middleware('jwt.auth');

        $this->report = new ReportService();
    }

    /**
     * @param int $reportTypeId
     * @param int|null $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function all(int $reportTypeId, int $userId = null)
    {
        if (is_null($userId)) {
            $userId = JwtVerification::$user['id'];
        }

        $data = $this->report->all($reportTypeId, $userId);

        return $this->errorsOrData($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function store(Request $request)
    {
        $this->report->create($request->all());

        return $this->errorsOrEmpty();
    }

}
